//ejercicio #1
function start1() {
    let octal = document.getElementById("entry1").value;
    alert("El número octal " + octal + " en decimal es: " + parseInt(octal, 8));
  }
  
  //ejercicio #2
  function start2() {
    let num1 = parseInt(document.getElementById("entry2").value);
    let num2 = parseInt(document.getElementById("entry3").value);
    alert(Calculator(num1, num2));
  }
  function Calculator(num1, num2) {
    return (
      "La suma de " +
      num1 +
      " + " +
      num2 +
      " es: " +
      (num1 + num2) +
      "\nLa resta de " +
      num1 +
      " - " +
      num2 +
      " es: " +
      (num1 - num2) +
      "\nLa multiplicación de " +
      num1 +
      " * " +
      num2 +
      " es: " +
      num1 * num2 +
      "\nLa división de " +
      num1 +
      " / " +
      num2 +
      " es: " +
      (num1 / num2).toFixed(2)
    );
  }
  